package com.hometech.festival.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "STAGES")
public class Stage {

    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;
    private String address;

    private long audienceCapacity;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "stage", cascade = CascadeType.ALL)
    private List<Concert> concerts;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getAudienceCapacity() {
        return audienceCapacity;
    }

    public void setAudienceCapacity(long audienceCapacity) {
        this.audienceCapacity = audienceCapacity;
    }

    public List<Concert> getConcerts() {
        return concerts;
    }

    public void setConcerts(List<Concert> concerts) {
        this.concerts = concerts;
    }
}
