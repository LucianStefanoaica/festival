package com.hometech.festival.models;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class ConcertParams {

    private LocalDateTime dateAndTime;

    private String genre;

    private List<Long> performerIds;

    private long stageId;

    private long numberOfTickets;

    private BigDecimal ticketPrice;

    public LocalDateTime getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(LocalDateTime dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public List<Long> getPerformerIds() {
        return performerIds;
    }

    public void setPerformerIds(List<Long> performerIds) {
        this.performerIds = performerIds;
    }

    public long getStageId() {
        return stageId;
    }

    public void setStageId(long stageId) {
        this.stageId = stageId;
    }

    public long getNumberOfTickets() {
        return numberOfTickets;
    }

    public void setNumberOfTickets(long numberOfTickets) {
        this.numberOfTickets = numberOfTickets;
    }

    public BigDecimal getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(BigDecimal ticketPrice) {
        this.ticketPrice = ticketPrice;
    }
}
