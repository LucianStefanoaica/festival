package com.hometech.festival.models;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "BANDS")
public class Band {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    private String genre;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "band", cascade = CascadeType.ALL)
    private List<Performer> members;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public List<Performer> getMembers() {
        return members;
    }

    public void setMembers(List<Performer> members) {
        this.members = members;
    }
}
