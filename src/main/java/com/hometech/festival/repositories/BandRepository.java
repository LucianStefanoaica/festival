package com.hometech.festival.repositories;

import com.hometech.festival.models.Band;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BandRepository extends JpaRepository<Band, Long> {
}
