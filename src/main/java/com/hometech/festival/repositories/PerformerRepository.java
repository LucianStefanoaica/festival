package com.hometech.festival.repositories;

import com.hometech.festival.models.Performer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PerformerRepository extends JpaRepository<Performer, Long> {

    @Query("SELECT p FROM Performer p WHERE p.firstName = ?1 AND p.lastName = ?2")
    List<Performer> findAllByName(String firstName, String lastName);

    @Query("SELECT p FROM Performer p WHERE p.band IS NULL")
    List<Performer> findAllNotInABand();


}
