package com.hometech.festival.repositories;

import com.hometech.festival.models.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TicketRepository extends JpaRepository<Ticket, Long> {

    @Query(value = "SELECT * FROM tickets WHERE concert_id = ?1 AND sold = FALSE LIMIT 1", nativeQuery = true)
    Ticket findFirstByConcertId(long id);


}
