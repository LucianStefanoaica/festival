package com.hometech.festival.controllers;

import com.hometech.festival.models.Performer;
import com.hometech.festival.models.SearchParams;
import com.hometech.festival.repositories.PerformerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api")
public class PerformerController {

    private PerformerRepository performerRepository;

    @PostMapping("/performers")
    @ResponseStatus(HttpStatus.OK)
    public Performer addNewSinglePerformer(@RequestBody Performer performer) {
        return performerRepository.save(performer);
    }

    @GetMapping("/performers")
    @ResponseStatus(HttpStatus.OK)
    public List<Performer> getAllPerformers() {
        return performerRepository.findAll();
    }

    @GetMapping("/single_performers")
    @ResponseStatus(HttpStatus.OK)
    public List<Performer> getAllSinglePerformers() {
        return performerRepository.findAllNotInABand();
    }

    @GetMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public List<Performer> searchForPerformers(@RequestBody SearchParams params) {
        return performerRepository.findAllByName(params.getFirstName(), params.getLastName());
    }

    @Autowired
    public void setPerformerRepository(PerformerRepository performerRepository) {
        this.performerRepository = performerRepository;
    }
}
