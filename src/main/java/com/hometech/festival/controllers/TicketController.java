package com.hometech.festival.controllers;

import com.hometech.festival.models.Concert;
import com.hometech.festival.models.ConcertParams;
import com.hometech.festival.models.Stage;
import com.hometech.festival.models.Ticket;
import com.hometech.festival.repositories.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/tickets")
public class TicketController {

    private TicketRepository ticketRepository;

    // TODO As a user I want to be able to buy a specific type of ticket
    @PostMapping("/buy")
    @ResponseStatus(HttpStatus.OK)
    public Ticket buyTicket(@RequestParam long concertId) {
        return getTicketAndSetAsSold(concertId);
    }

    private synchronized Ticket getTicketAndSetAsSold(long concertId) {
        Ticket ticket = ticketRepository.findFirstByConcertId(concertId);
        ticket.setSold(true);
        ticketRepository.save(ticket);
        return ticket;
    }

    // TODO As a user I want to find out which is the most likely type of ticket to be sold out
    public Concert mostLikelyConcertToBeSoldOut() {
        return null;
    }

    // TODO As a user I want to find out which is the most likely stage to be sold out
    public Stage mostLikelyLocationToBeSoldOut() {
        return null;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Ticket> getAllConcerts() {
        return ticketRepository.findAll();
    }

    @Autowired
    public void setTicketRepository(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }
}
