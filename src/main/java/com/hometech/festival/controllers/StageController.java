package com.hometech.festival.controllers;

import com.hometech.festival.models.Stage;
import com.hometech.festival.repositories.StageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/stages")
public class StageController {

    private StageRepository stageRepository;

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public Stage addNewStage(@RequestBody Stage stage) {
        return stageRepository.save(stage);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Stage> getAllStages() {
        return stageRepository.findAll();
    }

    @Autowired
    public void setStageRepository(StageRepository stageRepository) {
        this.stageRepository = stageRepository;
    }
}
