package com.hometech.festival.controllers;

import com.hometech.festival.models.Band;
import com.hometech.festival.repositories.BandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/bands")
public class BandController {

    private BandRepository bandRepository;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Band> getAllBands() {
        return bandRepository.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public Band addNewBand(@RequestBody Band band) {
        band.getMembers().forEach(bandMember -> {
            bandMember.setGenre(band.getGenre());
            bandMember.setBand(band);
        });

        return bandRepository.save(band);
    }

    @Autowired
    public void setBandRepository(BandRepository bandRepository) {
        this.bandRepository = bandRepository;
    }
}
