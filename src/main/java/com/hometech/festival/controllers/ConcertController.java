package com.hometech.festival.controllers;

import com.hometech.festival.models.*;
import com.hometech.festival.repositories.ConcertRepository;
import com.hometech.festival.repositories.PerformerRepository;
import com.hometech.festival.repositories.StageRepository;
import com.hometech.festival.repositories.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/concerts")
public class ConcertController {

    private ConcertRepository concertRepository;

    private PerformerRepository performerRepository;

    private StageRepository stageRepository;

    private TicketRepository ticketRepository;

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public Concert addNewConcert(@RequestBody ConcertParams params) {

        Concert concert = new Concert();

        concert.setGenre(params.getGenre());
        concert.setDateAndTime(params.getDateAndTime());

        setStageToConcert(concert, params);

        Concert theConcert = concertRepository.save(concert);

        List<Performer> performers = performerRepository.findAllById(params.getPerformerIds());

        performers.forEach(performer -> performer.addNewConcert(theConcert));

        addTicketsToDatabase(params.getNumberOfTickets(), theConcert, params.getTicketPrice());

        performerRepository.saveAll(performers);

        return theConcert;
    }

    private void setStageToConcert(Concert concert, ConcertParams params) {
        Optional<Stage> optional = stageRepository.findById(params.getStageId());

        if (optional.isPresent()) {
            Stage stage = optional.get();
            concert.setStage(stage);
        }
    }

    private void addTicketsToDatabase(long numberOfTickets, Concert concert, BigDecimal price) {
        List<Ticket> tickets = new ArrayList<>();
        for (long i = 1; i <= numberOfTickets; ++i) {
            Ticket ticket = new Ticket();
            ticket.setConcert(concert);
            ticket.setPrice(price);
            tickets.add(ticket);
        }
        ticketRepository.saveAll(tickets);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Concert> getAllConcerts() {
        return concertRepository.findAll();
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    public Concert deleteConcert(@RequestBody Concert concert) {
        concertRepository.delete(concert);
        return concert;
    }

    // todo As a user I want to know which is the most liked genre
    public String mostLikedGenre() {
        return null;
    }

    @Autowired
    public void setRepository(ConcertRepository repository) {
        this.concertRepository = repository;
    }

    @Autowired
    public void setPerformerRepository(PerformerRepository performerRepository) {
        this.performerRepository = performerRepository;
    }

    @Autowired
    public void setStageRepository(StageRepository stageRepository) {
        this.stageRepository = stageRepository;
    }

    @Autowired
    public void setTicketRepository(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }
}
